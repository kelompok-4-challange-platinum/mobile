Feature: Product Offer

    To ensure that User as buyer can or can not make an offer

    Background: buyer offering the product
        When buyer click product
        And buyer clik "Saya tertarik dan ingin nego" button
    
    @positive_case
    Scenario: T019-buyer can make an offer with valid input
        Given buyer already login
        When buyer input 0~10 digits in Price field
        And buyer click "Kirim" button
        Then buyer can offer the product
        And buyer can see message "Harga tawarmu berhasil dikirim ke penjual" pops up
        And "Saya tertarik dan ingin nego" button change to "Menunggu Respon Penjual"
    
    @negative_case
        Scenario: T020-buyer cannot make an offer when input Price more than 10 digits
        Given buyer already login
        When buyer input more than 10 digits in Price field
        And buyer click "Kirim" button
        Then buyer can not offer the product
        And buyer still on the product page
    
        Scenario: T021-buyer cannot make an offer when buyer not login
        Given buyer not login
        When buyer input 0~10 digits in Price field
        And buyer click "Kirim" button
        Then buyer can not offer the product
        And buyer redirected to Login page
        And buyer can see an error message "Silahkan login terlebih dahulu"
