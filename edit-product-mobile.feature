Feature: Edit Product on Secondhand Application
Background:
    Given the user is logged in to their account
    When user clicks the product list menu
    And the product list menu page is displayed
    And user clicks on the product they want to manage
  	And the application displays required fields for editing the product

@positive_case
    Scenario: T025-User wants to edit an existing product with valid details
    When there exists a product with the following details:
    | Field         | Value                  |
    | Nama Produk   | Gang4_Tas Laptop       |
    | Harga Produk  | 300000                 |
    | Kategori      | Komputer dan Aksesoris |
    | Lokasi        | Bandung                |
    | Deskripsi     | A practical laptop bag |
    | Gambar        | tas_laptop.jpg         |
    And user modifies the product description to "A laptop bag, easy to carry everywhere"
    And user saves the changes by clicking the "Perbarui Produk" button
    Then the product should be edited successfully

@negative_case
    Scenario: T026-User wants to edit an invalid price on an existing product with non-numeric characters
    When there exists a product with the following details:
    | Field         | Value                  |
    | Nama Produk   | Gang4_Tas Laptop       |
    | Harga Produk  | 300000                 |
    | Kategori      | Komputer dan Aksesoris |
    | Lokasi        | Bandung                |
    | Deskripsi     | A practical laptop bag |
    | Gambar        | tas_laptop.jpg         |
    And user modifies the product description to "345000 ab"
    And user saves the changes by clicking the "Perbarui Produk" button
    Then the product should be not edited