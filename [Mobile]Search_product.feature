Feature: Search Product

    To ensure that User can or can not search the product

    Background: user access the homepage on App
        Given user launched the App
        When user access Homepage
    
    @positive_case
    Scenario Outline: user can see the search result
        When user "<actions>" in "<places>"
        Then user can see products with "<results>" "<result places>"
        Examples:
            | case_id  | actions                                    | places                      | results                              | result_places         |
            | T014     | input valid keyword                        | Search box                  | valid keyword                        | under the Search box  |
            | T015     | input mix of upper and lower case keyword  | Search box                  | mix of upper and lower case keyword  | under the Search box  |
            | T016     | click Category                             | "Telusuri Kategori" section | same category                        | in Homepage           |
    
    @negative_case
    Scenario Outline: user can not see the search result
        When user input "<conditions>" keyword in Search box
        Then user can not see any product under the Search box
        Examples:
            | case_id | conditions |
            | T017   | wrong      |
            | T018   | empty      |

