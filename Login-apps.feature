Feature: Login
Background: User wants to Login
    Given User wants to Login

@positive_case
    Scenario: T005-User success Login
        When User input valid email
        And User input valid password
        And User click button Masuk
        Then User successfully Login
@negative_case
    Scenario: T006-User not success Register
        When User input not valid email
        And User input not valid password
        And User click button Daftar
        Then User will get message Invalid email and password