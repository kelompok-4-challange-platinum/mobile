Feature: Register
Background: User wants to Register
    Given User wants to Register

@positive_case
    Scenario: T002-User success Register
        When User input name
        And User input valid email
        And User input valid password
        And User input Number phone
        And User input Kota
        And User input Alamat
        And User click button Daftar
        Then User successfully Register
@negative_case
    Scenario: T003-User not success Register
        When User input name
        And User input email has been used
        And User input valid password
        And User input Number phone
        And User input Kota
        And User input Alamat
        And User click button Daftar
        Then User will get message Email has already taken
     