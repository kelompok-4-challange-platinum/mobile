Feature: Add Product on Secondhand Application
Background:
    Given the user is logged in to their account
    When When user clicks +Jual button to add a product
    And the application displays required fields for adding a new product

@positive_case
    Scenario: T022-User wants to add a product by entering valid product details
    When user enters valid product details in the required fields:
    | Field         | Value                  |
    | Nama Produk   | Gang4_Tas Laptop       |
    | Harga Produk  | 300000                 |
    | Kategori      | Komputer dan Aksesoris |
    | Lokasi        | Bandung                |
    | Deskripsi     | A practical laptop bag |
    And user uploads an image associated with the product name
    And user clicks the Terbitkan button 
    Then the product is added successfully

@positive_case
    Scenario: T023-User wants to preview a product before publishing it on the website.
    When user enters valid product details in the required fields:
    | Field         | Value                   |
    | Nama Produk   | Samsung Phone Case      |
    | Harga Produk  | 50500                   |
    | Kategori      | Handphone dan Aksesoris |
    | Lokasi        | Bandung                 |
    | Deskripsi     | A strong case           |
    And user uploads an image associated with the product name
    And user clicks the Preview button 
    Then the product is previewed successfully

@negative_case
    Scenario: T024-User wants to add a product but leaves the description field blank.
    When user enters valid product details in the required fields:
    | Field         | Value             |
    | Nama Produk   | Black Leather Bag |
    | Harga Produk  | 150000            |
    | Kategori      | Tas Pria          |
    | Lokasi        | Bandung           |
    | Deskripsi     |                    |
    And user uploads an image associated with the product name
    And user clicks the Terbitkan button to trigger error
    Then the product is not added